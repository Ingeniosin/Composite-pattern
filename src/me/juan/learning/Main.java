package src.me.juan.learning;

import src.me.juan.learning.entity.dragon.Dragon;

public class Main {
    public static void main(String[] args) {
        Dragon dragon = new Dragon();
        dragon.tick();
    }
}

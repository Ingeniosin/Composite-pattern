package src.me.juan.learning.entity.dragon;

import src.me.juan.learning.entity.Entity;

public class DragonLeg implements Entity {

    @Override
    public void tick() {
        System.out.println("DragonLeg ticked!");
    }

}

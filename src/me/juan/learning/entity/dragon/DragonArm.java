package src.me.juan.learning.entity.dragon;

import src.me.juan.learning.entity.Entity;

public class DragonArm implements Entity {
    @Override
    public void tick() {
        System.out.println("DragonArm ticked!");
    }
}

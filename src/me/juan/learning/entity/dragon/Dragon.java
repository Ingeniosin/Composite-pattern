package src.me.juan.learning.entity.dragon;

import src.me.juan.learning.entity.Entity;

import java.util.List;

public class Dragon implements Entity {

    private List<Entity> children = List.of(
      new DragonArm(),
      new DragonHead(),
      new DragonLeg()
    );

    @Override
    public void tick() {
        children.forEach(Entity::tick);
        System.out.println("Total Dragon ticked!");
    }

}
